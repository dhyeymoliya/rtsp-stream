let express = require('express');
let path = require('path');
Stream = require('node-rtsp-stream');
stream = new Stream({
    name: 'name',
    streamUrl: 'rtsp://admin:937610@103.249.232.36/Streaming/channels/101',
    wsPort: 9999
});
let app = express();
app.use('/*', express.static('static'));
app.all('/', (req, res, next) => {
    res.sendFile('index.html', {
        root: path.join(__dirname, 'static')
    });
});
app.listen(3402, () => {
    console.log('App started');
});

